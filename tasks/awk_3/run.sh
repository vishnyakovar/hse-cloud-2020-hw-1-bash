#!/usr/bin/env bash
awk '{AVG = ($2 + $3 + $4) / 3;
if (AVG >= 60) {
    if (AVG >= 80) {
        GRADE = "A"
    } else {
        GRADE = "B"
    }
} else {
    if (AVG >= 50) {
        GRADE = "C"
    } else {
        GRADE = "FAIL"
    }
}; print $0,":",GRADE
}'

