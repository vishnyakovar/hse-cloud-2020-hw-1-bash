#!/usr/bin/env bash
read N
TOTAL=0
COUNT=0
for ((i=0; i<N; i++)); do
    read X
    ((TOTAL+=X))
    ((COUNT++))
done
echo "scale=3; $TOTAL / $COUNT" | bc
