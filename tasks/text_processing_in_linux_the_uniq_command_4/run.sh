#!/usr/bin/env bash
uniq -c | sed 's/^ *//' | grep "^1 " | cut -f2- -d' ' | head -c-2
