#!/usr/bin/env bash
awk 'BEGIN{ORS=""; OFS=""}{if (NR % 2 == 1) {print $0,";"} else {print $0,"\n"}}'
