#!/usr/bin/env bash
printf "%.3f" $((cat; echo) | sed 's/^ *//g' | bc -l)
